# README #

### CONFIGURATION:

* server.port (default 8080)
* generator.url.service (default http://generator:8080/json/generate)

### ENDPOINTS:

##### static endpoint served as page
URL: {domain}/json/parse/first/{size}
> example URL: http://localhost:8082/json/parse/first/100000

##### static endpoint served as file
URL: {domain}/json/parse/first/{size}/file
> example URL: http://localhost:8082/json/parse/first/1000000/file

##### dynamic endpoint served as page
URL: {domain}/json/parse/second/{size}?options={options}
> example URL: http://localhost:8082/json/parse/second/10000?options=_id,geo_position.latitude,geo_position.longitude

##### dynamic endpoint served as file
URL: {domain}/json/parse/second/{size}/file?options={options}
> example URL: http://localhost:8082/json/parse/second/1000000/file?options=_id,geo_position.latitude,geo_position.longitude

### Possible options:
* _type
*   _id
*   key
*   name
*   fullName
*   iata_airport_code
*   type
*   country
*   geo_position.latitude
*   geo_position.longitude   
*   location_id
*   inEurope
*   countryCode
*   coreCountry
*   distance

