package com.nsq1.json.parser.client.generator.api;

import feign.Param;
import feign.RequestLine;

import java.util.List;

public interface GeneratorRestApi {
    @RequestLine("GET /{size}")
    List<ResponseEntity> request(@Param("size") long size);
}
