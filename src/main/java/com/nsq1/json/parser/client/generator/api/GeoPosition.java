package com.nsq1.json.parser.client.generator.api;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class GeoPosition {
    private double latitude;
    private double longitude;
}
