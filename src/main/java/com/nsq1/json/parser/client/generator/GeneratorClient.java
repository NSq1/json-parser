package com.nsq1.json.parser.client.generator;

import com.nsq1.json.parser.client.generator.api.GeneratorRestApi;
import com.nsq1.json.parser.client.generator.api.ResponseEntity;
import feign.Feign;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.List;

@Component
public class GeneratorClient {
    @Value("${generator.url.service}")
    private String generatorUrlService;

    private GeneratorRestApi target;

    public List<ResponseEntity> request(long size) {
        return target.request(size);
    }

    @PostConstruct
    public void init() {
        target = Feign.builder()
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .target(GeneratorRestApi.class, generatorUrlService);
    }
}
