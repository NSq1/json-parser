package com.nsq1.json.parser.rest;

import com.nsq1.json.parser.service.DynamicParsingService;
import com.nsq1.json.parser.service.FirstParsingService;
import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.function.Supplier;

@Controller
@RequestMapping("/json/parse")
public class RestApi {
    @Autowired
    private FirstParsingService firstParsingService;
    @Autowired
    private DynamicParsingService dynamicParsingService;

    private final Logger log = Logger.getLogger(this.getClass());

    @RequestMapping("/first/{size}")
    public @ResponseBody String parseFirst(@PathVariable long size) {
        return performAndLogTime(() -> firstParsingService.request(size));
    }

    @RequestMapping("/first/{size}/file")
    public void downloadFirst(@PathVariable long size, HttpServletResponse response) throws IOException {
        attachAsFile(size, response, () -> firstParsingService.request(size));
    }

    @RequestMapping("/second/{size}")
    public @ResponseBody String parseSecond(@PathVariable long size, @RequestParam(value = "options") String[] options) {
        return performAndLogTime(() -> dynamicParsingService.request(size, options));
    }

    @RequestMapping("/second/{size}/file")
    public void downloadSecond(@PathVariable long size, @RequestParam(value = "options") String[] options, HttpServletResponse response) throws IOException {
        attachAsFile(size, response, () -> dynamicParsingService.request(size, options));
    }

    private <K> K performAndLogTime(Supplier<K> operation) {
        long start = System.currentTimeMillis();
        K response = operation.get();
        long end = System.currentTimeMillis();
        log.info(String.format("Operation took %s ms", end - start));
        return response;
    }

    private void attachAsFile(@PathVariable long size, HttpServletResponse response, Supplier<String> stringSupplier) throws IOException {
        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", String.format("attachment;filename=file_%s.csv", size));
        ServletOutputStream out = response.getOutputStream();
        out.println(performAndLogTime(stringSupplier));
        out.flush();
        out.close();
    }
}
