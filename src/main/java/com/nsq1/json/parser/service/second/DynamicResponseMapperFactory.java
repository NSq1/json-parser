package com.nsq1.json.parser.service.second;

import com.google.common.base.Function;
import com.nsq1.json.parser.client.generator.api.ResponseEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class DynamicResponseMapperFactory {
    @Autowired
    private ResponseEntityAccessors responseEntityAccessors;

    public Function<ResponseEntity, List<Object>> from(String[] definitions) {
        List<? extends java.util.function.Function<ResponseEntity, ?>> accessors = Arrays.stream(definitions)
                .map(DynamicResponseMapperFactory.this.responseEntityAccessors::accessor)
                .collect(Collectors.toList());
        return input -> accessors.parallelStream()
                .map(accessor -> accessor.apply(input))
                .collect(Collectors.toList());
    }
}
