package com.nsq1.json.parser.service.second;

import com.google.common.base.Joiner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.function.Function;

@Component
public class DynamicResponseToCsvConverter implements Function<List<Object>, String> {
    private static final String NULL_TEXT = "";
    public static final char CSV_VALUE_SEPARATOR = ',';

    public String apply(List<Object> response) {
        return Joiner.on(CSV_VALUE_SEPARATOR)
                .useForNull(NULL_TEXT)
                .join(response);
    }
}