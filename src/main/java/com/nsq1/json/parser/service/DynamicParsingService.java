package com.nsq1.json.parser.service;

import com.nsq1.json.parser.client.generator.GeneratorClient;
import com.nsq1.json.parser.service.second.DynamicResponseMapperFactory;
import com.nsq1.json.parser.service.second.DynamicResponseToCsvConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class DynamicParsingService {
    private static final String CSV_RECORD_DELIMITER = "\n";

    @Autowired
    private GeneratorClient generatorClient;
    @Autowired
    private DynamicResponseMapperFactory responseMapperFactory;
    @Autowired
    private DynamicResponseToCsvConverter toCsvConverter;

    public String request(long size, String[] definition) {
        return generatorClient.request(size)
                .parallelStream()
                .map(responseMapperFactory.from(definition))
                .map(toCsvConverter)
                .collect(Collectors.joining(CSV_RECORD_DELIMITER));
    }
}
