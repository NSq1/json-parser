package com.nsq1.json.parser.service;

import com.nsq1.json.parser.client.generator.GeneratorClient;
import com.nsq1.json.parser.service.first.FirstResponseMapper;
import com.nsq1.json.parser.service.first.FirstResponseToCsvLineConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
public class FirstParsingService {
    private static final String CSV_RECORD_DELIMITER = "\n";

    @Autowired
    private GeneratorClient generatorClient;
    @Autowired
    private FirstResponseMapper firstResponseMapper;
    @Autowired
    private FirstResponseToCsvLineConverter manualFirstResponseToCsv;

    public String request(long size) {
        return generatorClient.request(size)
                .parallelStream()
                .map(firstResponseMapper)
                .map(manualFirstResponseToCsv)
                .collect(Collectors.joining(CSV_RECORD_DELIMITER));
    }
}
