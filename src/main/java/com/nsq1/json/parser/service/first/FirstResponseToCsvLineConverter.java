package com.nsq1.json.parser.service.first;

import com.google.common.base.Joiner;
import com.nsq1.json.parser.service.first.dto.FirstResponse;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FirstResponseToCsvLineConverter implements Function<FirstResponse, String> {
    private static final String NULL_TEXT = "";
    public static final char CSV_VALUE_SEPARATOR = ',';

    public String apply(FirstResponse response) {
        return Joiner.on(CSV_VALUE_SEPARATOR)
                .useForNull(NULL_TEXT)
                .join(response.get_type(),
                        response.get_id(),
                        response.getName(),
                        response.getType(),
                        response.getLatitude(),
                        response.getLongitude());
    }
}
