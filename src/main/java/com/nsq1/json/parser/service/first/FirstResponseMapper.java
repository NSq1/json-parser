package com.nsq1.json.parser.service.first;

import com.nsq1.json.parser.client.generator.api.ResponseEntity;
import com.nsq1.json.parser.service.first.dto.FirstResponse;
import org.springframework.stereotype.Component;

import java.util.function.Function;

@Component
public class FirstResponseMapper implements Function<ResponseEntity, FirstResponse> {

    public FirstResponse apply(ResponseEntity input) {
        return new FirstResponse() {

            @Override
            public Object get_type() {
                return input.get_type();
            }

            @Override
            public Object get_id() {
                return input.get_id();
            }

            @Override
            public Object getName() {
                return input.getName();
            }

            @Override
            public Object getType() {
                return input.getType();
            }

            @Override
            public Object getLatitude() {
                return input.getGeo_position().getLatitude();
            }

            @Override
            public Object getLongitude() {
                return input.getGeo_position().getLatitude();
            }
        };
    }
}
