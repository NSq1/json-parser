package com.nsq1.json.parser.service.first.dto;

public interface FirstResponse {
    public Object get_type();

    public Object get_id();

    public Object getName();

    public Object getType();

    public Object getLatitude();

    public Object getLongitude();
}
