package com.nsq1.json.parser.service.second;

import com.google.common.collect.ImmutableMap;
import com.nsq1.json.parser.client.generator.api.GeoPosition;
import com.nsq1.json.parser.client.generator.api.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Optional;
import java.util.function.Function;

@Component
public class ResponseEntityAccessors {
    private static final ImmutableMap<String, Function<ResponseEntity, ?>> ACCESSORS = ImmutableMap.<String, Function<ResponseEntity, ?>>builder()
            .put("_type", ResponseEntity::get_type)
            .put("_id", ResponseEntity::get_id)
            .put("key", ResponseEntity::getKey)
            .put("name", ResponseEntity::getName)
            .put("fullName", ResponseEntity::getFullName)
            .put("iata_airport_code", ResponseEntity::getIata_airport_code)
            .put("type", ResponseEntity::getType)
            .put("country", ResponseEntity::getCountry)
            .put("geo_position.latitude", responseEntity -> Optional.ofNullable(responseEntity.getGeo_position()).map(GeoPosition::getLatitude).orElse(null))
            .put("geo_position.longitude", responseEntity -> Optional.ofNullable(responseEntity.getGeo_position()).map(GeoPosition::getLongitude).orElse(null))
            .put("location_id", ResponseEntity::getLocation_id)
            .put("inEurope", ResponseEntity::isInEurope)
            .put("countryCode", ResponseEntity::getCountryCode)
            .put("coreCountry", ResponseEntity::isCoreCountry)
            .put("distance", ResponseEntity::getDistance)
            .build();

    public Function<ResponseEntity, ?> accessor(String key) {
        return ACCESSORS.get(key);
    }
}
