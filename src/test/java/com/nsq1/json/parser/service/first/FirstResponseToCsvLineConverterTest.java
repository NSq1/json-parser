package com.nsq1.json.parser.service.first;

import com.nsq1.json.parser.service.first.dto.FirstResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FirstResponseToCsvLineConverterTest {
    private static final String NAME = "name";
    private static final Object LONGITUDE = "longitude";
    private static final String _TYPE = "_type";
    private static final String _ID = "_id";
    private static final String TYPE = "type";
    private static final String LATITUDE = "latitude";

    @InjectMocks
    private FirstResponseToCsvLineConverter firstResponseToCsvLineConverter;

    @Test
    public void shouldCorrectlyJoinElements() {
        String expected = _TYPE + "," + _ID + "," + NAME + "," + TYPE + "," + LATITUDE + "," + LONGITUDE;
        FirstResponse response = Mockito.mock(FirstResponse.class);
        when(response.get_type()).thenReturn(_TYPE);
        when(response.get_id()).thenReturn(_ID);
        when(response.getName()).thenReturn(NAME);
        when(response.getType()).thenReturn(TYPE);
        when(response.getLatitude()).thenReturn(LATITUDE);
        when(response.getLongitude()).thenReturn(LONGITUDE);

        String actual = firstResponseToCsvLineConverter.apply(response);

        assertThat(actual, is(expected));
    }

    @Test
    public void shouldNotPrintNullTexts() {
        String expected = ",," + NAME + ",,,";
        FirstResponse response = Mockito.mock(FirstResponse.class);
        when(response.getName()).thenReturn(NAME);

        String actual = firstResponseToCsvLineConverter.apply(response);

        assertThat(actual, is(expected));
    }
}